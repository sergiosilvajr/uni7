package com.apps.sergiosilvajr.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class JavaMainActivity extends AppCompatActivity {
    public static final String EXTRA = "data";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java);

        Button button = findViewById(R.id.my_first_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(JavaMainActivity.this, MainActivity.class);
                intent.putExtra(EXTRA, "Meu nome");
                startActivity(intent);
            }
        });
    }
}
